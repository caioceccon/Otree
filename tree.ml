type 'a tree = Folha
             | Ramif of ('a tree * 'a * 'a tree)
let cria_arvore x = Ramif (Folha, x, Folha)

let rec insere_valor t x =

  match t with
  | Folha ->  cria_arvore x
  | Ramif (esq, v, dir) ->
          if x < v then
            Ramif (insere_valor esq x, v, dir)
          else
            Ramif (esq, v, insere_valor dir x)

let rec imprime_em_ordem t print =

  match t with
  | Folha -> ()
  | Ramif (esq, v, dir) ->
            imprime_em_ordem esq print;
            print v;
            imprime_em_ordem dir print

let map_tree f t =
  let rec map t acc =

  match t with
  | Folha -> acc
  | Ramif (esq, v, dir) ->
         let res = f v in
         let acc = insere_valor acc res in
         let acc = map esq acc in
         map dir acc in
    map  Folha

let rec fold_tree f t z =

  match t with
  | Folha -> z
  | Ramif (esq, v, dir) ->
           let y = f z v in
           let w = fold_tree f y esq in
           fold_tree f w dir

(* FUNCTOR *)
(*God is real, unless declared integer.*)
